//
// LSM303Test.ino
//

// References:
// - https://github.com/pololu/lsm303-arduino
// - example Serial.ino
//

//
// Wiring:
// Arduino   LSM303 board
//    5V   -  VIN
//   GND   -   GND
//   SDA   -   SDA
//   SCL   -   SCL
//

#include <Wire.h>
#include <LSM303.h>

LSM303 compass;
// from Calibrate.ino
LSM303::vector<int16_t> running_min = {32767, 32767, 32767}, running_max = {-32768, -32768, -32768};

char report[80];

void setup() {

  Serial.begin(9600);
  Wire.begin();
  compass.init();
  compass.enableDefault();

  /*
  Calibration values; the default values of +/-32767 for each axis
  lead to an assumed magnetometer bias of 0. Use the Calibrate example
  program to determine appropriate values for your particular unit.
  */
#if 0
  compass.m_min = (LSM303::vector<int16_t>){-32767, -32767, -32767};
  compass.m_max = (LSM303::vector<int16_t>){+32767, +32767, +32767};
#else
  compass.m_min = (LSM303::vector<int16_t>){ -333, -633, -986 };
  compass.m_max = (LSM303::vector<int16_t>){  390, -105, -622 };
#endif
}

// loop() from Calibrate.ino
void calibrate() {

  compass.read();

  running_min.x = min(running_min.x, compass.m.x);
  running_min.y = min(running_min.y, compass.m.y);
  running_min.z = min(running_min.z, compass.m.z);

  running_max.x = max(running_max.x, compass.m.x);
  running_max.y = max(running_max.y, compass.m.y);
  running_max.z = max(running_max.z, compass.m.z);

  snprintf(report, sizeof(report), "min: {%+6d, %+6d, %+6d}    max: {%+6d, %+6d, %+6d}",
    running_min.x, running_min.y, running_min.z,
    running_max.x, running_max.y, running_max.z);
  Serial.println(report);

  delay(100);
}

void loop() {

#if 0
  calibrate();
#else
  compass.read();

  snprintf(report, sizeof(report), "A: %6d %6d %6d    M: %6d %6d %6d",
    compass.a.x, compass.a.y, compass.a.z,
    compass.m.x, compass.m.y, compass.m.z);
  Serial.println(report);

  // from Heading.ino
#if 1
  /*
  The default vector is chosen by the library to point along the
  surface of the PCB, in the direction of the top of the text on the
  silkscreen. This is the +X axis on the Pololu LSM303D carrier and
  the -Y axis on the Pololu LSM303DLHC, LSM303DLM, and LSM303DLH
  carriers.
  */
  float heading = compass.heading();
#else
  /*
  To use a different vector as a reference, use the version of heading()
  that takes a vector argument; for example, use ...
  */
  float heading = compass.heading((LSM303::vector<int>){0, 0, 1});
#endif

  Serial.println(heading);
#endif

  //delay(100);
  delay(1000);
}
