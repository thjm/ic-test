/**************************************************************************/
/*!
This is a demo for the Adafruit MCP9808 breakout
----> http://www.adafruit.com/products/1782
Adafruit invests time and resources providing this open source code,
please support Adafruit and open-source hardware by purchasing
products from Adafruit!
*/
/**************************************************************************/

#include <Wire.h>
#include "Adafruit_MCP9808.h"

// Create the MCP9808 temperature sensor object
Adafruit_MCP9808 tempsensor = Adafruit_MCP9808();

void setup() {

  Serial.begin(9600);
  Serial.println("*** MCP9808 Demo ***");

  // I2C base address is 0x18, so range is 0x18 ... 0x1f
  // Make sure the sensor is found, you can also pass in a different I2C
  // address with tempsensor.begin(0x19) for example
  if (!tempsensor.begin(0x18)) {
    Serial.println("Couldn't find MCP9808!");
    while (1);
  }

  //tempsensor.shutdown_wake(0);

  // just a test, read the sensor's Id
  uint16_t id = tempsensor.read16(MCP9808_REG_DEVICE_ID);

  Serial.print("Id: 0x"); Serial.println(id, HEX);

  // read the other registers
  uint16_t data = tempsensor.read16(MCP9808_REG_UPPER_TEMP);
  Serial.print("Upper temp: "); Serial.println(data);

  data = tempsensor.read16(MCP9808_REG_LOWER_TEMP);
  Serial.print("Lower temp: "); Serial.println(data);

  data = tempsensor.read16(MCP9808_REG_CRIT_TEMP);
  Serial.print("Critical temp: "); Serial.println(data);

  data = tempsensor.read16(MCP9808_REG_MANUF_ID);
  Serial.print("Manufacturer Id: "); Serial.println(data);
}

void loop() {

  Serial.println("Wake up MCP9808.... "); // wake up MSP9808 - power consumption ~200 mikro Ampere

  tempsensor.shutdown_wake(0);   // don't remove this line! required before reading temp

  // Read and print out the temperature, then convert to *F
  float c = tempsensor.readTempC();
  float f = c * 9.0 / 5.0 + 32;

  Serial.print("Temp: "); Serial.print(c); Serial.print("*C\t");
  Serial.print(f); Serial.println("*F");

  delay(250);

  Serial.println("Shutdown MCP9808.... ");

  tempsensor.shutdown_wake(1); // shutdown MCP9808 - power consumption ~0.1 mikro Ampere

  delay(2000);
}
