# IC Test Code

Some simple test programs to test some features of selected chips (IC) within
the Arduino framework.

## ADS1x15

Requires ```Adafruit_ADS1015.h```

- ADS1115 = 16-bit version
- ADS1015 = 12-bit version

## BMP085test

BMP085 Barometric Pressure & Temp Sensor

Requires ```Adafruit_BMP085.h```

## DHTtester

DHT humidity/temperature sensors

Requires ```DHT.h```

## DS1307Test

DS1307 TWI real time clock (RTC)

- Requires ```Streaminh.h```
- Requires ```RTClib.h```

## LSM303Test

Example sketch for a LSM303DLH (or similar) acceleration and
magnetometer sensor (compass).

See also:
 https://github.com/pololu/lsm303-arduino

## MCP472X

Example sketch for the Adafruit MCP4725 DAC breakout board.

Requires ```Adafruit_MCP4725.h```

## MCP9808test

Demo for the Adafruit MCP9808 temperature sensor breakout

Requires ```Adafruit_MCP9808.h```
