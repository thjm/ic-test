
//
// File   : DS1307Test.ino
//
// Purpose: test frame for Dallas DS1307 I2C Real Time Clock
//
// Information concerning the wiring of the Tiny RTC I2C breakout module see:
// http://blog.simtronyx.de/kurz-vorgestellt-zeit-und-datum-mit-dem-tiny-rtc-modul-ds1307-echtzeituhr-chip/
//

//
// Programming example from:
// https://learn.adafruit.com/ds1307-real-time-clock-breakout-board-kit/arduino-library
// This uses the module from Adafruit
//
// The required RTClib is from Adafruit:
// https://github.com/adafruit/RTClib
// Which is a fork of the RTClib from JeeLab:
// https://github.com/jcw/rtclib
//

// http://arduiniana.org/libraries/streaming/
#include <Streaming.h>

#include <Wire.h>     // RTClib won't work without this
#include <RTClib.h>

// UART baud rate
#define UART_BAUD_RATE  9600

RTC_DS1307 rtc;

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete

void setup () {
  /* Initialize serial output at UART_BAUD_RATE bps */
  Serial.begin(UART_BAUD_RATE);
  Serial << F("Starting ...") << endl;

  Wire.begin();

  // reserve 100 bytes for the inputString:
  inputString.reserve(100);

  rtc.begin();

  if ( !rtc.isrunning() ) {

    Serial << F("RTC is NOT running!") << endl;

    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));

    DateTime now = rtc.now();

    Serial << F("RTC set to: ");
    printDateTime(now);

    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }
  else
    Serial << F("RTC is running!") << endl;
}

/** Print date and time to the Serial console.
  *
  * @todo use stream implementation
  */
static void printDateTime(DateTime& dt) {

  Serial << dt.year() << '/'
         << ((dt.month()<10) ? "0" : "") << dt.month() << '/'
         << ((dt.day()<10) ? "0" : "") << dt.day() << ' '
         << ((dt.hour()<10) ? "0" : "") << dt.hour() << ':'
         << ((dt.minute()<10) ? "0" : "") << dt.minute() << ':'
         << ((dt.second()<10) ? "0" : "") << dt.second()
         << endl;
}

void loop () {

  DateTime now = rtc.now();

  printDateTime(now);

  Serial.print(F(" since midnight 1/1/1970 = "));
  Serial.print(now.unixtime());
  Serial.print("s = ");
  Serial.print(now.unixtime() / 86400L);
  Serial.println("d");

#if 0
  // calculate a date which is 7 days and 30 seconds into the future
  DateTime future (now.unixtime() + 7 * 86400L + 30);

  Serial.print(F(" now + 7d + 30s: "));
  printDateTime( future );
#endif

  Serial.println();

  for ( int i=0; i<30; ++i ) {

    // print the string when a newline arrives:
    if (stringComplete) {
      Serial.println(inputString);

      now = rtc.now();

      // parse the string and set the clock
#if 1
      if (inputString.length() == 7) {
        if ( inputString[0] == 'D' ) {
          DateTime set( inputString.substring(5,7).toInt() + 2000,
                        inputString.substring(3,5).toInt(),
                        inputString.substring(1,3).toInt(),
                        now.hour(), now.minute(), now.second());
          rtc.adjust( set );
        }
        if ( inputString[0] == 'T' ) {
          DateTime set( now.year(), now.month(), now.day(),
                        inputString.substring(1,3).toInt(),
                        inputString.substring(3,5).toInt(),
                        inputString.substring(5,7).toInt());
          rtc.adjust( set );
        }
#else
      if (inputString.length() == 6) {
        DateTime set( inputString.substring(4,6).toInt() + 2000,
                      inputString.substring(2,4).toInt(),
                      inputString.substring(0,2).toInt(),
                      now.hour(), now.minute(), now.second());
        rtc.adjust( set );
      } else if (inputString.length() == 12) {
        DateTime set( inputString.substring(4,6).toInt() + 2000,
                      inputString.substring(2,4).toInt(),
                      inputString.substring(0,2).toInt(),
                      inputString.substring(6,8).toInt(),
                      inputString.substring(8,10).toInt(),
                      inputString.substring(10,12).toInt());
        rtc.adjust( set );
#endif
      } else {
        Serial.println(F("Invalid Date/Time SET format!"));
      }

      // clear the string:
      inputString = "";
      stringComplete = false;
    }

    delay(100);
  }
}

/** SerialEvent occurs whenever a new data comes in the hardware serial RX.
  * This routine is run between each time loop() runs, so using delay inside loop can
  * delay response.  Multiple bytes of data may be available.
  */
void serialEvent() {

  while (Serial.available()) {

    // get the new byte:
    char inChar = (char)Serial.read();

    switch (inChar ) {

      // if the incoming character is a newline, set a flag
      // so the main loop can do something about it:
      case '\n':
        if ( inputString.length() ) stringComplete = true;
        break;

      case 'd':
      case 'D':
        // clear the string:
        inputString = "";
        Serial.print('\nEnter date in DDMMYY format:');
        inputString += 'D';
        break;

      case 'h':
      case 'H':
      case '?':
        Serial.println("D - set date, T - set time\n");
        break;

      case 't':
      case 'T':
        // clear the string:
        inputString = "";
        Serial.print('\nEnter time in HHMMSS format:');
        inputString += 'T';
        break;

      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
        // add digits to the inputString:
        inputString += inChar;
        break;
    }

    if (stringComplete) return;
  }
}
